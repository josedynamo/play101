package controllers

import play.api._
import play.api.mvc._
import models.WebFrameworks
import play.api.libs.ws.WS
import play.api.libs.concurrent.Execution.Implicits._


object Application extends Controller {

  def index(year: Int) = Action {
    Ok(views.html.index("Hello SCALA BCN! It's the year " + year))
  }

  def redirect = Action {
    Redirect(routes.JavaController.helloJava())
  }

  def getFW = Action {
    val wfs = WebFrameworks.findAll
    Ok(views.html.frameworks(wfs))
  }

  def addFW(id: Int, name: String, language: String) = Action {
    WebFrameworks.add(id, name, language)
    Redirect(routes.Application.getFW)
  }

  def goMOB = Action.async {
    WS.url("http://www.meetup.com").get.map {
      response => Ok(response.body).as("text/html")
    }
  }

  def getTopArtists = Action.async {
    val apiKey=Play.current.configuration.getString("last.fm.api.key").get
    WS.url("http://ws.audioscrobbler.com/2.0/")
      .withQueryString("method" -> "user.gettopartists",
        "user" -> "josebcn",
        "api_key" -> apiKey,
        "format" -> "json").get().map {
      response => Ok(response.body).as("json")
    }


  }

}