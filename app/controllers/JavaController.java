package controllers;

import play.mvc.Result;
import play.mvc.Controller;

/**
 * Created by joseramonrivera on 26/03/14.
 */
public class JavaController extends Controller {
    public static Result helloJava() {
       return ok("hello from Java!");
    }

}
